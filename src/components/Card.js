import React, {useEffect, useState} from 'react';
import Button from "./Button";

import CartItem from "./CartItem";
function Card({data , handleModalOpen, ElementAdd , FavArray , DeleteStar }) {

    return (
        <>
            {
                data.map((dataElement) => (
                    <CartItem key={dataElement.id} dataElement={dataElement} handleModalOpen={handleModalOpen} ElementAdd={ElementAdd} FavArray={FavArray} DeleteStar={DeleteStar} />
                ))
            }
        </>
    );
}

export default Card;