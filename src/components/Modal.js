import React , {useEffect} from 'react';
import Button from "./Button";

function Modal({ handleModalOpen , onBuy , AddToShopCart, buyElement , title , text , ModalType , handleModalDelete , removeFromCart , deleteModal}) {

    const handleOverlayClick = (event) => {
        if (event.target.classList.contains('overlay')) {
            if(ModalType === `BUY`) {
                handleModalOpen();
            }  else{
                handleModalDelete();
            }
        }
    };

    return (
        <div className="overlay" onClick={handleOverlayClick}  >
            <div className="modal-content" >
                <div className="modal-content_title">
                    <h2>{title}</h2>
                    <span onClick={handleModalOpen}>&times;</span>
                </div>
                <p>{text}</p>
                { ModalType === "BUY" &&
                <div className="modal-content_btn-group">
                    <Button handleModalOpen={handleModalOpen} onBuy={onBuy} state={`onBuy`} title={'Ok'} AddToShopCart={AddToShopCart}  buyElement={buyElement}/>
                    <Button handleModalOpen={handleModalOpen}  state={`onCancel`} title={'Close'} />
                </div>
                }
                {
                 ModalType === "DELETE" &&
                    <div className="modal-content_btn-group">
                        <Button handleModalDelete={handleModalDelete} onBuy={onBuy} state={`onCancelDelete`} title={'Ok'} AddToShopCart={AddToShopCart}  buyElement={buyElement}  removeFromCart={removeFromCart} deleteModal={deleteModal}/>
                        <Button handleModalDelete={handleModalDelete}  state={`onCancelClose`} title={'Close'}  deleteModal={deleteModal}/>
                    </div>
                }
            </div>
        </div>

    );
}

export default Modal;