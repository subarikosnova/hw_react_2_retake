import React, {useState , useEffect} from 'react';
import {FaRegStar} from "react-icons/fa";
import Button from "./Button";

function CartItem({dataElement, handleModalOpen, ElementAdd ,FavArray , DeleteStar }) {

    const [stars, setStars] = useState(false);
    const toggleStar = () => {
        setStars(!stars)
    }

    useEffect(() => {
        const storedStar = JSON.parse(localStorage.getItem("star")) || [];
       if (storedStar) {
           storedStar.forEach((el) => {
               if (dataElement.id === el.id) {
                   toggleStar()
               }
           })
       }
    }, []);


    return (
            <>
            <div className="card" key={dataElement.id}>
                <div className="star">
                    <FaRegStar className={`${stars && 'active'}`} onClick= {() =>{
                            toggleStar();
                            !stars ? FavArray(dataElement) : DeleteStar(dataElement)
                        }}/>
                </div>
                <img src={dataElement.image} alt="logo"/>
                <h2>{dataElement.name}</h2>
                <p>Article: {dataElement.article}</p>
                <p>Price: {dataElement.price} $</p>
                <p>Color {dataElement.color}</p>
                <Button handleModalOpen={handleModalOpen}  title={"Buy"} item={dataElement} state={`buy`}  ElementAdd={ElementAdd}  />
            </div>
            </>
    );
}

export default CartItem;