import React from 'react';

function Button({title  , handleModalOpen  , item ,state , ElementAdd , AddToShopCart, buyElement , handleModalDelete , removeFromCart  , deleteModal}) {
    return (
    <>
        { state === "buy" && <button onClick={() =>{
            try{
                ElementAdd(item)
                handleModalOpen()
            }catch(er) {
                console.log(er)
            }

        }}>
            {title}
        </button>}
        { state === "onBuy" && <button onClick={() =>{
            try{
            handleModalOpen() ;
                AddToShopCart(buyElement)

            }catch(er) {
                console.log(er)
            }

        }}>
            {title}
        </button>}

        { state === "onCancel" && <button onClick={() =>{
            handleModalOpen()
            console.log(`onCancel`)
        }}>
            {title}
        </button>}


        { state === "onCancelDelete" && <button onClick={() =>{
            handleModalDelete()
            removeFromCart(deleteModal)
            console.log(`onCancel`)
        }}>
            {title}
        </button>}

        { state === "onCancelClose" && <button onClick={() =>{
            handleModalDelete()
            console.log(`onCancel`)
        }}>
            {title}
        </button>}

    </>
);
}

export default Button;